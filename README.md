<h1>PySpeedtest</h1>
A simple tool to check bandwidth and ping over time written in Python 3 which writes results to a file
<br>
Prerequisites:<br>
&nbsp;&nbsp;&nbsp;&nbsp;Python 3.x<br>
&nbsp;&nbsp;&nbsp;&nbsp;Speedtest CLI<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-pip install speedtest-cli<br>

Usage:<br>
&nbsp;&nbsp;&nbsp;&nbsp;optional arguments:<br>
&nbsp;&nbsp;&nbsp;&nbsp;-h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;show this help message and exit<br>
&nbsp;&nbsp;&nbsp;&nbsp;-c COUNT, --count COUNT<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The number of tests to run (Default=1)<br>
&nbsp;&nbsp;&nbsp;&nbsp;-f FILETYPE, --filetype FILETYPE
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The format of the output file (csv, json, dynamodb). Default is json<br>

To use dynamodb as a filetype you will need to have credentials set up on your local machine and a dynamodb table set up within AWS.