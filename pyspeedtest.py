import os
import json
import csv
import datetime
import time
import argparse
import boto3
import decimal

SPEEDTEST_CMD = r'speedtest-cli'
RESULTS_FILE = 'speedtest'
AWS_REGION = "eu-west-1"
DYNAMODB_TABLE = "speedtest"

class FileManager():
    def __init__(self,file_extension):
        self.filename = RESULTS_FILE + "." + file_extension
        self.file_extension = file_extension
        self.results = []
        if self.file_extension == "csv":
            fieldnames = ['timestamp', 'ping', 'download', 'upload']
            if os.path.exists(self.filename)==False:
                with open(self.filename, mode='w') as results_file:
                    csv_writer = csv.writer(results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                    csv_writer.writerow(fieldnames) 
        else:
            if os.path.exists(self.filename):
                with open(self.filename) as json_file:
                    self.results = json.load(json_file)

    def writefile(self,result):   
        if self.file_extension == "csv":
            with open(self.filename, mode='a', newline='') as results_file:
                csv_writer = csv.writer(results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                csv_writer.writerow([result["timestamp"], result["ping"], result["download"], result["upload"]])
        elif self.file_extension == "dynamodb":
              self.write_to_dynamodb(result)
        else:
            with open(self.filename, 'w') as results_file:
                self.results.append(result)
                json.dump(self.results, results_file)

    def write_to_dynamodb(self,result):
        dynamodb = boto3.resource('dynamodb',region_name=AWS_REGION)
        table = dynamodb.Table(DYNAMODB_TABLE)
        with table.batch_writer() as batch:
              batch.put_item(
                  Item={
                    'timestamp': result["timestamp"],
                    'ping': decimal.Decimal(str(result["ping"])),
                    'upload': decimal.Decimal(str(result["upload"])),
                    'download': decimal.Decimal(str(result["download"]))
                  }
              )
    

def main():
  
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--number", help = "The number of tests to run (Default=1)")
    parser.add_argument("-f", "--filetype", help = "The format of the output file (csv, json, dynamodb). Default is json" )
    parser.add_argument("-t", "--time", help = "number of hours to run tests for.")
    args = parser.parse_args()

    #get number of test to run from command line arg(If not defined defaults to 1)
    number_of_tests = 1
    if args.number and represents_int(args.number):
        number_of_tests = int(args.number)
    if args.filetype:
        file_manager= FileManager(args.filetype)
    else:
        file_manager= FileManager("json")
    if args.time and represents_int(args.time):
        t_end = time.time() + (60 * 60) * int(args.time)
        while time.time()<t_end:
            print ("running tests until: " + str(t_end) + " Current time: " + str(time.time()))
            file_manager.writefile(get_ping_results())
    else:
        for test_number in range(number_of_tests): # run the test x times
            print("Running test: " + str(test_number+1) + " of " + str(number_of_tests))
            file_manager.writefile(get_ping_results())

#run a speedtest using speedtest-cli  
def get_ping_results():
    date_time_obj = datetime.datetime.now()
    result = {
        "timestamp":date_time_obj.strftime("%d-%m-%Y %H:%M:%S"),
        "ping":0,
        "download":0,
        "upload":0
    }
    with os.popen(SPEEDTEST_CMD + ' --simple') as speedtest_output:
        print("\tTimestamp: " + str(result["timestamp"]))  
        for line in speedtest_output:
            label, value, unit = line.split()
            print("\t"+label + " : " + str(value)+ " " + str(unit))  
            if 'Ping' in label:
                result["ping"] = float(value)
            elif 'Download' in label:
                result["download"] = float(value)
            elif 'Upload' in label:
                result["upload"] = float(value)
    return result

#check if string represents int
def represents_int(s):
    try: 
        int(s)
        return True
    except ValueError:
        print(s + " is not a valid integer value. Using Default")
        return False

if __name__ == '__main__':
    main()